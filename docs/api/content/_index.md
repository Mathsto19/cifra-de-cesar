# Cifra de César

## 1. Introdução

Na criptografia, a cifra de César, também conhecida como cifra de troca, o código de César ou a troca de César, é uma das técnicas de criptografia mais simples e famosas. É uma senha de substituição em que cada letra do texto é substituída por outra letra, que aparece um número fixo de vezes no alfabeto abaixo dela. Por exemplo, para uma troca de três dígitos, A será substituído por D, B se tornará E e assim por diante. O método foi nomeado após Júlio César, e César o usou para se comunicar com seus generais. O processo de criptografar a cifra de César é geralmente incorporado como parte de um esquema mais complexo (como a cifra de Virgínia) e continua a ter aplicações modernas, como em sistemas ROT13. Como todas as senhas alternativas de uma letra, a senha Caesar é fácil de decifrar e, na verdade, basicamente não oferece segurança na comunicação.

![](cesar.jpg)

## 2. História

A senha de César leva o nome de Júlio César e, de acordo com Suetônio, ele a usa como uma troca de três dígitos para proteger informações de importância militar. Embora o uso desse esquema por César seja o primeiro a ser documentado, é bem sabido que outras senhas alternativas foram usadas antes. A validade da senha de César neste momento não é clara, mas pode ser bastante segura, especialmente porque a maioria dos inimigos de César são analfabetos e outros acreditam que a informação está escrita em um idioma estrangeiro desconhecido. Supondo que o inimigo possa ler a mensagem, desde então não houve registro de nenhuma técnica para resolver a simples substituição de senha. O registro mais recente que sobreviveu pode ser rastreado até o trabalho de análise de frequência de Alcundi, descoberto no mundo árabe no século IX.

No verso da mezuzá, uma senha de César com alteração de local é usada para criptografar o nome de Deus. Este pode ser um legado da mezuzá não permitido pelos judeus nos tempos antigos. As letras da senha formam um nome sagrado que pode controlar as forças do mal.
No século 19, anúncios pessoais em jornais às vezes eram usados ​​para trocar mensagens criptografadas usando esquemas de criptografia simples. Kahn (1967) descreveu no The Times alguns exemplos de comunicação secreta de amantes criptografada pela cifra de César. Mesmo em 1915, a cifra de César ainda estava em uso: o exército russo a usava em vez de cifras mais complexas, que se mostraram difíceis para suas tropas entenderem; no entanto, os criptoanalistas na Alemanha e na Áustria tiveram pouca dificuldade em decifrar suas mensagens.

Hoje, os códigos César podem ser encontrados em brinquedos infantis, como anéis decodificadores. Uma troca Caesar de 13 bits também foi realizada no algoritmo ROT13, que é um método simples de ofuscar texto amplamente difundido no UNIX, usado para ofuscar texto (como o fim de uma piada ou spoiler), mas não como um método de criptografia...
A senha Vigenère utiliza a senha Caesar, que tem uma troca diferente em cada posição do texto, o valor de troca é definido por meio de palavras-chave repetidas. Se a palavra-chave for tão longa quanto a mensagem e não for repetida e selecionada aleatoriamente, então é uma senha de uso único. Se seus usuários mantiverem a palavra-chave em segredo, ela não poderá ser decifrada. Palavras-chave mais curtas do que a mensagem como "vitória completa".

![](cifra_desenho.png)

## 3. Criptografia

A criptografia de segurança virtual é a conversão de dados de um formato legível em um formato codificado. Os dados criptografados só podem ser lidos ou processados ​​após a descriptografia.
A criptografia é o elemento básico da segurança de dados. Esta é a maneira mais fácil e importante de garantir que as informações do sistema do computador não sejam roubadas e lidas por pessoas que desejam usá-las para fins maliciosos.
Usuários individuais e grandes empresas usam amplamente a criptografia de segurança de dados para proteger as informações do usuário enviadas entre o navegador e o servidor. Essas informações podem incluir tudo, desde detalhes de pagamento a informações pessoais. O software de criptografia de dados, também conhecido como algoritmo ou código de criptografia, é usado para desenvolver um esquema de criptografia que, teoricamente, só pode ser quebrado com uma grande quantidade de poder de processamento.

![](criptografia.png)

## 4. Como fuinciona

Quando informações ou dados são compartilhados na Internet, eles passam por uma série de dispositivos em rede ao redor do mundo, que fazem parte da Internet pública. Quando transmitidos na Internet pública, os dados correm o risco de serem destruídos ou roubados por hackers. Para evitar essa situação, os usuários podem instalar software ou hardware específico para garantir a transmissão segura de dados ou informações. Esses processos são chamados de criptografia de segurança de rede.
A criptografia envolve a conversão de texto simples legível por humanos em texto incompreensível, o que é conhecido como texto cifrado. Essencialmente, isso significa pegar dados legíveis e transformá-los de forma que pareçam aleatórios. A criptografia envolve o uso de uma chave criptográfica, um conjunto de valores matemáticos com os quais tanto o remetente quanto o destinatário concordam. O destinatário usa a chave para descriptografar os dados, transformando-os de volta em texto simples legível.

Quanto mais complexa for a chave criptográfica, mais segura será a criptografia, pois é menos provável que terceiros a descritografem por meio de ataques de força bruta (ou seja, tentar números aleatórios até que a combinação correta seja adivinhada). A criptografia também é usada para proteger senhas. Os métodos de criptografia de senha codificam a sua senha de forma que ela fique ilegível por hackers.

![](Como_funciona.jpg)

## 5. Quando a criptografia deve ser usada

Esse recurso é amplamente utilizado com o intuito de evitar invasões de pessoas mal-intencionadas às mensagens e aos arquivos salvos em diferentes formatos. A criptografia também é eficiente para impossibilitar o roubo de dados ou de senhas que circulam em dispositivos com acesso à Internet. O uso desse poderoso mecanismo de segurança ocorrer em diversas ocasiões nas quais haja a necessidade de proteção do usuário da rede.

Um exemplo clássico é quando ocorre a perda, o roubo ou o extravio de computadores, notebooks, tablets, smartphones, entre outros dispositivos em que há o arquivamento de dados eletrônicos. A maioria das pessoas que passam por essas situações começa a se preocupar com o risco de algum desconhecido ter acesso a todos os arquivos armazenados nesses equipamentos.

Em alguns casos, é possível apagar remotamente todos os dados para reduzir as chances de invasão alheia, o que evita que usuários indesejados acessem os dados contidos nesses dispositivos. Mas somente os gadgets portáteis com iOS, Windows Phone e Android têm essa possibilidade, que inclusive deve estar previamente configurada. Nos computadores ou notebooks, isso não pode ser feito em decorrência do sistema operacional em funcionamento.

Logo, o manuseio de dados nesses equipamentos precisa priorizar o mesmo cuidado que é tomado com o armazenamento de arquivos na web ou com o envio de informações pela Internet. É claro que não é necessário bloquear o acesso a todos os documentos guardados em um disco rígido, no entanto ter mais cautela com os dados sensíveis arquivados pode evitar o surgimento de problemas.

![](Como_usar.jpg)

## 6. Desafios encontrados

Infelizmente a template da criação do site não está funcinando na minha aplicação. Tentei remover o template, mas sempre dava problema no pipeline, então resolvi deixar por mais que seja um grande problema.
